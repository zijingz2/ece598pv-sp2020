use crate::block::{Block,Header};
use crate::crypto::hash::{H256,Hashable};
use crate::transaction::{SignedTransaction};
use crate::crypto::merkle::{MerkleTree};
use std::collections::HashMap;
use log::{debug};


pub struct Blockchain {
    blkchn : HashMap<H256, Block>,
    tip: H256,
    blklen : HashMap<H256, u32>,
    orphanblk : HashMap<H256, Block>,
}

impl Blockchain {
    /// Create a new blockchain, only containing the genesis block
    pub fn new() -> Self {
        let mut blkchn = HashMap::new();
        let mut blklen = HashMap::new();
        let nonce : u32 = 0;
        let mut dif : [u8; 32] = [0;32];
        for i in 2..32 {
            dif[i]=255;
        }
        dif[2]=63;
        let tmsdp = 0i64; // change to milisecond
        let tt : Vec<SignedTransaction> = vec![];
        let mk = MerkleTree::new(&tt);
        let mkrt : H256 = mk.root();
        let hdif = H256::from(dif);
        let hd = Header{parent : hdif, nonce : nonce, difficulty : hdif , timestamp : tmsdp, merkle_root : mkrt}; // hash of any block is smaller than dif
        let blk = Block{head : hd, content : tt};
        let blkhash = Hashable::hash(&blk);
        let orphanblk = HashMap::new();
        blkchn.insert(blkhash, blk);
        blklen.insert(blkhash, 0u32);
        Blockchain{blkchn : blkchn, tip : blkhash, blklen : blklen, orphanblk : orphanblk}
    }

    /// Insert a block into blockchain
    pub fn insert(&mut self, block: &Block) {
        let blkhash = Hashable::hash(block);
        if self.blkchn.contains_key(&block.head.parent) {
            self.blkchn.insert(blkhash, Clone::clone(block));
            self.blklen.insert(blkhash, self.blklen[&block.head.parent]+1);
            let tmsdp = chrono::Utc::now().timestamp_nanos();
            let sizez = bincode::serialize(&block).unwrap().len();
            let lensz = self.blklen[&blkhash];
            debug!("blkdelay:{}, size:{}, len:{} Insert succeed.", tmsdp-block.head.timestamp, sizez, lensz);
            if self.blklen[&blkhash] > self.blklen[&self.tip] {
                self.tip = blkhash;
            }
        }        
    }

    pub fn insert_orphan(&mut self, block: &Block) {
        let blkhash = Hashable::hash(block);
        if !self.orphanblk.contains_key(&blkhash) {
            self.orphanblk.insert(blkhash, Clone::clone(block));
        }
    }

    pub fn remove_orphan(&mut self, blkhash: &H256) {
        if self.orphanblk.contains_key(blkhash) {
            self.orphanblk.remove(blkhash);
        }
    }

    /// Get the tip of the longest chain
    pub fn tip(&self) -> H256 {
        self.tip
    }

    pub fn orphanpool (&self) -> &HashMap<H256, Block> {
        &self.orphanblk
    }

    pub fn get_tip_difficulty(&self) -> H256 {
        self.blkchn[&self.tip].head.difficulty
    }
    
    pub fn try_update_tip(&mut self, newhash: H256) {
        if self.blklen[&newhash] > self.blklen[&self.tip] {
            self.tip = Clone::clone(&newhash);
        }
    }

    pub fn get_chain_length(&self) -> u32 {
        self.blklen[&self.tip]
    }

    pub fn check_blk_in_hashmap(&self, key : &H256) -> bool {
        self.blkchn.contains_key(key)
    }

    pub fn get_certain_block(&self, ctn : &H256) -> Block {
        Clone::clone(&self.blkchn[ctn])
    }


    #[cfg(any(test, test_utilities))]
    pub fn all_blocks_in_longest_chain(&self) -> Vec<H256> {
        unimplemented!() // same longest chain? may useful between competing miners
    }
}

#[cfg(any(test, test_utilities))]
mod tests {
    use super::*;
    use crate::block::test::generate_random_block;
    use crate::crypto::hash::Hashable;

    #[test]
    fn insert_one() {
        let mut blockchain = Blockchain::new();
        let genesis_hash = blockchain.tip();
        let block = generate_random_block(&genesis_hash);
        blockchain.insert(&block);
        assert_eq!(blockchain.tip(), block.hash());

    }
}
