use serde::{Serialize,Deserialize};
use ring::signature::{Ed25519KeyPair, Signature};
use crate::crypto::hash::{H256, H160, Hashable};
// use rand::Rng;

#[derive(Serialize, Deserialize, Debug, Default, Clone)]
pub struct SignedTransaction {
    pub transaction : Transaction,
    pub pub_key     : Vec<u8>,
    pub signature   : Vec<u8>,
    pub trans_hash  : H256,
}
impl Hashable for SignedTransaction {
    fn hash(&self) -> H256 {
        let t_ser = bincode::serialize(&self.transaction).unwrap();
        ring::digest::digest(&ring::digest::SHA256, &t_ser).into()
    }
}

impl SignedTransaction {
    pub fn get_hash(&self) -> H256 {
        self.trans_hash
    }
}


#[derive(Serialize, Deserialize, Debug, Default, Clone)]
pub struct Transaction {
    pub input_hash          : Vec<H256>,
    pub input_index         : Vec<u32>,
    pub output_recipient    : Vec<H160>,
    pub output_value        : Vec<u32>,
}

impl Hashable for Transaction {
    fn hash(&self) -> H256 {
        let t_ser = bincode::serialize(self).unwrap();
        ring::digest::digest(&ring::digest::SHA256, &t_ser).into()
    }
}

impl Transaction {
    pub fn new(
        input_hash          : H256, 
        input_index         : u32, 
        output_recipient    : Vec<H160>, 
        output_value        : Vec<u32>,

    ) -> Self {
        Transaction {
            input_hash          : vec![input_hash],
            input_index         : vec![input_index],
            output_recipient    : output_recipient,
            output_value        : output_value,
        }
    }
    pub fn default_init() -> Self {
        let input_hash = H256::from([0 ; 32]);
        let output_recipient = H160::from(&input_hash);
        Transaction {
            input_hash          : vec![input_hash],
            input_index         : vec![0],
            output_recipient    : vec![output_recipient],
            output_value        : vec![0],
        }
    }
}

/// Create digital signature of a transaction
pub fn sign(t: &Transaction, key: &Ed25519KeyPair) -> Signature {
    let t_ser = bincode::serialize(&t).unwrap();
    key.sign(&t_ser)
}

/// Verify digital signature of a transaction, using public key instead of secret key
pub fn verify(t: &Transaction, public_key: &[u8], signature: &[u8]) -> bool {
    let t_ser = bincode::serialize(&t).unwrap();
    let public_key_struct = ring::signature::UnparsedPublicKey::new(&ring::signature::ED25519, public_key);
    public_key_struct.verify(&t_ser, signature).is_ok()
}

#[cfg(any(test, test_utilities))]
mod tests {
    use super::*;
    use crate::crypto::key_pair;
    use ring::signature::{KeyPair};

    pub fn generate_random_transaction() -> Transaction {
        // let mut rng = rand::thread_rng();
        // let nonce : u32 = rng.gen();
        Transaction::default_init()
    }

    #[test]
    fn sign_verify() {
        let t = generate_random_transaction(); // not random now
        let key = key_pair::random();
        let signature = sign(&t, &key);
        assert!(verify(&t, &key.public_key().as_ref().to_vec()[..], &signature.as_ref().to_vec()[..]));
    }
}
