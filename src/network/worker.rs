use crate::block::Block;
use std::collections::HashMap;
use crate::transaction::{SignedTransaction,verify};
use super::message::Message;
use super::peer;
use crate::network::server::Handle as ServerHandle;
use crate::blockchain::Blockchain;
use crate::crypto::hash::{H256, H160, Hashable};
use crossbeam::channel;
use log::{info, debug, warn};
use std::convert::TryInto;

use std::thread;
use std::sync::{Arc,Mutex};
use byteorder::{WriteBytesExt, BigEndian};


#[derive(Clone)]
pub struct Context {
    msg_chan: channel::Receiver<(Vec<u8>, peer::Handle)>,
    num_worker: usize,
    server: ServerHandle,
    blkchain : Arc<Mutex<Blockchain>>,
    mempool : Arc<Mutex<HashMap<H256,SignedTransaction>>>,
    statemap : Arc<Mutex<HashMap<H256, HashMap<(H256, u32), (H160, u32)>>>>,
    veckey : Arc<Mutex<Vec<[u8;100]>>>,
    key_len : usize,
}

pub fn new(
    num_worker: usize,
    msg_src: channel::Receiver<(Vec<u8>, peer::Handle)>,
    server: &ServerHandle,
    blockchain: &Arc<Mutex<Blockchain>>,
    mempool : &Arc<Mutex<HashMap<H256,SignedTransaction>>>,
    statemap : &Arc<Mutex<HashMap<H256, HashMap<(H256, u32), (H160, u32)>>>>,
    veckey : &Arc<Mutex<Vec<[u8;100]>>>,
    key_len : usize,
) -> Context {
    Context {
        msg_chan: msg_src,
        num_worker,
        server: server.clone(),
        blkchain : Arc::clone(blockchain),
        mempool : Arc::clone(mempool),
        statemap : Arc::clone(statemap),
        veckey : Arc::clone(veckey),
        key_len : key_len,
    }
}

impl Context {
    pub fn start(self) {
        let num_worker = self.num_worker;
        for i in 0..num_worker {
            let cloned = self.clone();
            thread::spawn(move || {
                cloned.worker_loop();
                warn!("Worker thread {} exited", i);
            });
        }
            // // test pkcs8 key process
            // let key_25519 = Ed25519KeyPair::from_pkcs8(&self.key[0..self.key_len]).unwrap();
            // info!("uid_addr: {:?}",key_25519.public_key().as_ref());
    }

    fn worker_loop(&self) {
        loop {
            let msg = self.msg_chan.recv().unwrap();
            let (msg, peer) = msg;
            let msg: Message = bincode::deserialize(&msg).unwrap();
            match msg {
                Message::Ping(nonce) => {
                    debug!("Ping: {}", nonce);
                    peer.write(Message::Pong(nonce.to_string()));
                }
                Message::Pong(nonce) => {
                    debug!("Pong: {}", nonce);
                }
                Message::NewBlockHashes(newblkhash) => {
                    let blkchn = self.blkchain.lock().unwrap();
                    for i in newblkhash {
                        if !blkchn.check_blk_in_hashmap(&i) {
                            peer.write(Message::GetBlocks(vec![i]));
                            debug!("I received a new blk hash broadcast");
                        }
                    }
                }
                Message::GetBlocks(reqstblk) => {
                    let blkchn = self.blkchain.lock().unwrap();
                    for i in reqstblk {
                        if blkchn.check_blk_in_hashmap(&i) {
                            peer.write(Message::Blocks(vec![blkchn.get_certain_block(&i)]));
                            debug!("I sent out one blocks");
                        }
                    }
                }
                Message::Blocks(recvblk) => {
                    let mut blkchn = self.blkchain.lock().unwrap();
                    for i in recvblk {
                        let blkhash = Hashable::hash(&i);
                        if !blkchn.check_blk_in_hashmap(&blkhash) { // if its new block
                            if blkchn.check_blk_in_hashmap(&i.head.parent) { // if parent in blockchain
                                let mut statemap = self.statemap.lock().unwrap();
                                if verify_block(&i, &blkchn, &statemap[&i.head.parent]) { 
                                    // insert and add state for current block
                                    blkchn.insert(&i);
                                    let mut new_state : HashMap<(H256, u32), (H160, u32)> = Clone::clone(&statemap[&i.head.parent]);
                                    update_state(&i, &mut new_state);
                                    verify_mempool(&new_state, &mut self.mempool.lock().unwrap());
                                    statemap.insert(blkhash, Clone::clone(&new_state));
                                    display_state(&new_state);
                                    //find children
                                    let mut found : u32;
                                    let mut newhash = blkhash;
                                    let mut newblock : Block = Clone::clone(&i);
                                    loop {
                                        found = 0;
                                        for (child_hash, child) in blkchn.orphanpool().iter() {
                                            if child.head.parent == newhash {
                                                if verify_block(&child, &blkchn, &new_state) { //verify child and insert
                                                    info!("found a child block.");
                                                    found = 1;
                                                    newhash = Clone::clone(child_hash);
                                                    newblock = Clone::clone(child);
                                                    break;
                                                }
                                            }
                                        }
                                        // add child to blockchain
                                        if found == 1{
                                            blkchn.insert(&newblock); // beware the lifetime of new_state, it will be cleared after if block
                                            let mut new_state: HashMap<(H256, u32), (H160, u32)> = Clone::clone(&statemap[&newblock.head.parent]);
                                            update_state(&newblock, &mut new_state);
                                            verify_mempool(&new_state, &mut self.mempool.lock().unwrap());
                                            statemap.insert(newhash, Clone::clone(&new_state));
                                            display_state(&new_state);
                                            blkchn.remove_orphan(&newhash);
                                        } else {break;}
                                    }
                                    blkchn.try_update_tip(Clone::clone(&newhash));
                                    //broadcast the last block
                                    self.server.broadcast(Message::NewBlockHashes(vec![newhash]));
                                    debug!("I inserted and broadcasted the latest block.");
                                }
                            }
                            else { // if parent not in blockchain, add as orphan and ask for parent
                                blkchn.insert_orphan(&i);
                                peer.write(Message::GetBlocks(vec![i.head.parent]));
                                debug!("I received an orphan block and I asked for its parent.");
                            }
                        }
                    }
                }
                Message::NewTransactionHashes(newtranshash) => {
                    let mmp = self.mempool.lock().unwrap();
                    for i in newtranshash {
                        if !mmp.contains_key(&i) {
                            peer.write(Message::GetTransactions(vec![i]));
                            debug!("I received a new transaction hash broadcast and requested.");
                        }
                    }
                }
                Message::GetTransactions(reqsttrans) => {
                    let mmp = self.mempool.lock().unwrap();
                    for i in reqsttrans {
                        if mmp.contains_key(&i) {
                            peer.write(Message::Transactions(vec![Clone::clone(&mmp[&i])]));
                            debug!("I sent out one transaction upon request.");
                        }
                    }
                }
                Message::Transactions(recvtrans) => {
                    let blkchn = self.blkchain.lock().unwrap();
                    let mut mmp = self.mempool.lock().unwrap();
                    let statemap = self.statemap.lock().unwrap();
                    let state = &statemap[&blkchn.tip()];
                    for i in recvtrans {
                        let transhash = i.get_hash();
                        //check first
                        let eql = verify_transaction(&i, &state);
                        if eql == 0u32 {
                            if !mmp.contains_key(&transhash) {
                                mmp.insert(transhash, i);
                            }
                            self.server.broadcast(Message::NewTransactionHashes(vec![transhash]));
                            debug!("I broadcasted the newly received transaction.");
                        }
                    }
                }
                Message::SharePKCS8Keys(sharedkey) => {
                    let vckey = self.veckey.lock().unwrap();
                    let vckey_len = vckey.len();
                    let mut found = 0;
                    let mut msgs : [u8; 32] = [0; 32];
                    for i in 0..vckey_len {
                        msgs.copy_from_slice(&vckey[i][0..32]);
                        if msgs == sharedkey {
                            found = 1;
                        }
                    }
                    if found == 0 {
                        peer.write(Message::GetPKCS8Keys(sharedkey));
                        debug!("I receive a key broadcast and request a new pkcs8key.");
                    }
                }
                Message::GetPKCS8Keys(reqstkey) => {
                    let vckey = self.veckey.lock().unwrap();
                    let vckey_len = vckey.len();
                    let mut msgs : [u8; 32] = [0; 32];
                    for i in 0..vckey_len {
                        msgs.copy_from_slice(&vckey[i][0..32]);
                        if msgs == reqstkey {
                            let mut pcks8key : [u32; 32] = [0; 32];
                            for j in 0..25 {
                                pcks8key[j]=vckey[i][4*j].into(); // read entend doesnt work
                                pcks8key[j]=pcks8key[j]*256 + vckey[i][4*j+1] as u32;
                                pcks8key[j]=pcks8key[j]*256 + vckey[i][4*j+2] as u32;
                                pcks8key[j]=pcks8key[j]*256 + vckey[i][4*j+3] as u32; 
                            }
                            peer.write(Message::PKCS8Keys(pcks8key));
                            debug!("I send one pkcs8key: {:?}.",pcks8key);
                        }
                    }
                }
                Message::PKCS8Keys(recvkey) => {
                    let mut vckey = self.veckey.lock().unwrap();
                    let mut rdkey : [u8; 100] = [0; 100];
                    let mut rdvec : Vec<u8> = vec![];
                    for j in 0..25 {
                        rdvec.write_u32::<BigEndian>(recvkey[j]).unwrap();
                    }
                    rdkey.copy_from_slice(&rdvec);
                    vckey.push(rdkey);
                    let vckey_len = vckey.len();
                    for i in 0..vckey_len {
                        let mut msgs : [u8; 32] = [0; 32];
                        msgs.copy_from_slice(&vckey[i][0..32]);
                        self.server.broadcast(Message::SharePKCS8Keys(msgs));
                    }
                    debug!("I receive one pkcs8key: {:?}.",recvkey);
                }
            }
        }
    }
}



pub fn verify_mempool (state: &HashMap<(H256, u32), (H160, u32)>, mempool: &mut HashMap<H256,SignedTransaction>) {
    let mut mark_for_delete : HashMap<H256,u32> = HashMap::new();
    for (transhash, trans) in mempool.iter(){
        let exit_err = verify_transaction(trans, state);
        if exit_err !=0 {
            mark_for_delete.insert(Clone::clone(transhash), 1);
            info!("after state update, found invalid transaction and delete it.");
        }
    }
    for (transhash, _nonce) in mark_for_delete.iter(){
        mempool.remove(transhash);
    }
}



pub fn update_state (i: &Block, state: &mut HashMap<(H256, u32), (H160, u32)>) {
    let mut index = 0;
    for j in &i.content{
        if index == 0 {
            state.insert((j.trans_hash,j.transaction.input_index[0]),(j.transaction.output_recipient[0],j.transaction.output_value[0]));
            info!("The mining reward is added to state.");
        }
        else {
            let lnn = j.transaction.output_recipient.len();
            for iti in 0..lnn {
                if !state.contains_key(&(j.trans_hash,iti.try_into().unwrap())){
                    state.insert((j.trans_hash,iti.try_into().unwrap()),(j.transaction.output_recipient[iti],j.transaction.output_value[iti]));
                    info!("The transaction is executed: index:{}, value:{}.", iti, j.transaction.output_value[iti]);
                }
            }
            state.remove(&(j.transaction.input_hash[0],j.transaction.input_index[0]));
        }
        index = index + 1;
    }
}


pub fn display_state (state: &HashMap<(H256, u32), (H160, u32)>) {
    info!("-------State display-------");
    for (inputside, outputside) in state {
        info!("UTXO: hash:{:?}, index:{}, value:{}, recp:{:?}",inputside.0, inputside.1, outputside.1, outputside.0);
    }
}




pub fn verify_block(i: &Block, blkchn: &Blockchain, state: &HashMap<(H256, u32), (H160, u32)> ) -> bool {
    let mut index = 0;
    let blkhash = Hashable::hash(i);
    if blkhash < blkchn.get_tip_difficulty() { // check block vadility, if not stop propagation               
        for j in &i.content {  // validate transactions
            if index == 0 {
                if j.transaction.output_value[0] != 50 {
                    info!("This block claims invalid mining reward.");
                    return false;
                }
            }
            else {
                if verify_transaction(&j, &state) !=0 { return false;}
                for k in &i.content {
                    if j.transaction.input_hash[0] == k.transaction.input_hash[0] && j.transaction.input_index[0] == k.transaction.input_index[0] && j.trans_hash != k.trans_hash {
                        info!("The input of this transaction is spend twice in this block.");
                        return false;
                    }
                }
            }
            index = index + 1;
        }
    } else {
        info!("The difficulty of this block does not fit the blockchain.");
        return false;
    }
    info!("The block passes the checks.");
    return true;
}



pub fn verify_transaction(j: &SignedTransaction, state: &HashMap<(H256, u32), (H160, u32)>) -> u32 {  // validate signature, ownership, spending
    let mut exit_err = 0;
    if verify(&j.transaction,&j.pub_key[..],&j.signature[..]) {
        let key = &(j.transaction.input_hash[0],j.transaction.input_index[0]);
        if state.contains_key(key) {
            let mut addr : [u8; 32] = [0; 32];
            addr[..].copy_from_slice(&j.pub_key[..]);
            let addr = H160::from(&H256::from(addr));
            if state[key].0 == addr {
                let lnn = j.transaction.output_value.len();
                let mut tot_spend : u32 = 0;
                for iti in 0..lnn {
                    tot_spend = tot_spend + j.transaction.output_value[iti];
                }
                if state[key].1 != tot_spend {
                    exit_err = 4;
                    info!("This transaction spending is different with input."); // invalid transaction
                }
            } else {
                exit_err = 3;
                info!("This transaction claims ownership of others wallet."); // invalid ownership
            }

        } else {
            exit_err = 2;
            info!("This transaction claims ownership of an unknown wallet."); // invalid ownership or double spending
        }
    } else { 
        exit_err = 1; // invalid signature
        info!("This transaction is not signed correctly.");
    }
    exit_err
}