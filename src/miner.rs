use std::collections::HashMap;
use crate::transaction::{Transaction,SignedTransaction,sign};
use crate::blockchain::Blockchain;
use crate::block::{Block,Header};
use crate::crypto::hash::{H256, H160, Hashable};
use crate::crypto::merkle::{MerkleTree};
use crate::network::server::Handle as ServerHandle;
use crate::network::message::Message;
use log::info;
use ring::signature::{Ed25519KeyPair, KeyPair};
use super::*;
use crate::worker::{update_state, verify_mempool, display_state};

use crossbeam::channel::{unbounded, Receiver, Sender, TryRecvError};
use std::time;
use std::sync::{Arc,Mutex};
use std::thread;
use rand::Rng;

enum ControlSignal {
    Start(u64,u32), // the number controls the lambda of interval between block generation
    Exit,
}

enum OperatingState {
    Paused,
    Run(u64),
    ShutDown,
}

pub struct Context {
    /// Channel for receiving control signal
    control_chan: Receiver<ControlSignal>,
    operating_state: OperatingState,
    server: ServerHandle,
    blkchain : Arc<Mutex<Blockchain>>,
    blk_mined : u32,
    mempool : Arc<Mutex<HashMap<H256,SignedTransaction>>>,
    uid : u32,
    key : Ed25519KeyPair,
    statemap : Arc<Mutex<HashMap<H256, HashMap<(H256, u32), (H160, u32)>>>>,
}

#[derive(Clone)]
pub struct Handle {
    /// Channel for sending signal to the miner thread
    control_chan: Sender<ControlSignal>,
}

pub fn new(
    server: &ServerHandle,
    blockchain: &Arc<Mutex<Blockchain>>,
    mempool : &Arc<Mutex<HashMap<H256,SignedTransaction>>>,
    key : Ed25519KeyPair,
    statemap : &Arc<Mutex<HashMap<H256, HashMap<(H256, u32), (H160, u32)>>>>,
) -> (Context, Handle) {
    let (signal_chan_sender, signal_chan_receiver) = unbounded();

    let ctx = Context {
        control_chan: signal_chan_receiver,
        operating_state: OperatingState::Paused,
        server: server.clone(),
        blkchain : Arc::clone(blockchain),
        blk_mined : 0,
        mempool : Arc::clone(mempool),
        statemap : Arc::clone(statemap),
        uid : 0,
        key : key,
    };

    let handle = Handle {
        control_chan: signal_chan_sender,
    };

    (ctx, handle)
}

impl Handle {
    pub fn exit(&self) {
        self.control_chan.send(ControlSignal::Exit).unwrap();
    }

    pub fn start(&self, lambda: u64, uid: u32) {
        self.control_chan
            .send(ControlSignal::Start(lambda,uid))
            .unwrap();
    }

}

impl Context {
    pub fn start(mut self) {
        thread::Builder::new()
            .name("miner".to_string())
            .spawn(move || {
                self.miner_loop();
            })
            .unwrap();
        info!("Miner initialized into paused mode");
    }

    fn handle_control_signal(&mut self, signal: ControlSignal) {
        match signal {
            ControlSignal::Exit => {
                let i = self.blkchain.lock().unwrap().get_chain_length();
                info!("Miner shutting down");
                info!("Miner mined {} blocks.", self.blk_mined);
                info!("Miner stopped with {} blocks.", i+1);
                self.operating_state = OperatingState::ShutDown;
            }
            ControlSignal::Start(i,j) => {
                info!("Miner starting in continuous mode with lambda {}", i);
                self.operating_state = OperatingState::Run(i);
                self.uid = j;
            }
        }
    }

    fn miner_loop(&mut self) {
        // main mining loop
        loop {
            // check and react to control signals
            match self.operating_state {
                OperatingState::Paused => {
                    let signal = self.control_chan.recv().unwrap();
                    self.handle_control_signal(signal);
                    continue;
                }
                OperatingState::ShutDown => {
                    return;
                }
                _ => match self.control_chan.try_recv() {
                    Ok(signal) => {
                        self.handle_control_signal(signal);
                    }
                    Err(TryRecvError::Empty) => {}
                    Err(TryRecvError::Disconnected) => panic!("Miner control channel detached"),
                },
            }
            if let OperatingState::ShutDown = self.operating_state {
                return;
            }

            // actual mining
            let mut blkchn = self.blkchain.lock().unwrap();
            let blktip = blkchn.tip();
            let hdif = blkchn.get_tip_difficulty();
            let tmsdp = chrono::Utc::now().timestamp_nanos(); // change to nanosecond

            //get mining reward for self
            
            let mut tt : Vec<SignedTransaction> = vec![];
            let mut tk : Vec<H256> = vec![];
            let pubkey = self.key.public_key().as_ref().to_vec();
            let mut uid_addr : [u8 ; 32] = [0 ; 32];
            uid_addr[..].copy_from_slice(self.key.public_key().as_ref());
            // info!("uid_addr: {:?}",self.key.public_key().as_ref());
            let uid_addr = H160::from(&H256::from(uid_addr));
            let mut rng = rand::thread_rng();
            let rand1 : u32 = rng.gen();
            let trans = Transaction::new(H256::from([0;32]),rand1,vec![uid_addr],vec![50]); //ICO 50
            let trans_signature = sign(&trans, &self.key);
            let signa = trans_signature.as_ref().to_vec();
            let transhash = Hashable::hash(&trans);
            let sign_trans = SignedTransaction{transaction : trans, pub_key : pubkey, signature : signa, trans_hash : transhash};
            tt.push(sign_trans);
            // gather transaction, create merkle root
            let mut num = 1;
            let mut mmp = self.mempool.lock().unwrap();
            for (key, val) in mmp.iter() {
                tk.push(Clone::clone(key));
                tt.push(Clone::clone(val));
                if num > 4 { // 5 transactions per block or less
                    break;
                }
                else{
                    num = num + 1;
                }
            }
            let mk = MerkleTree::new(&tt);
            let mkrt : H256 = mk.root();

            //main loop // only check one nonce each time
            let mut rng = rand::thread_rng();
            let nonce = rng.gen();
            let nbhash : Message;

            let hd = Header{parent : blktip, nonce : nonce, difficulty : hdif , timestamp : tmsdp, merkle_root : mkrt};
            let bk = Block{head : hd, content : tt};
            let blkhash = Hashable::hash(&bk);
            
            if blkhash < hdif {
                nbhash = Message::NewBlockHashes(vec![blkhash]);
                blkchn.insert(&bk);
                let mut statemap = self.statemap.lock().unwrap();
                let mut new_state : HashMap<(H256, u32), (H160, u32)> = Clone::clone(&statemap[&bk.head.parent]);
                update_state(&bk, &mut new_state);
                verify_mempool(&new_state, &mut mmp);
                statemap.insert(blkhash, Clone::clone(&new_state));
                display_state(&new_state);
                
                self.server.broadcast(nbhash);
                self.blk_mined = self.blk_mined + 1;
                for i in tk {
                    mmp.remove(&i);
                }
                info!("I mined 1 block and I broadcasted.");
            }
            // mining finished

            if let OperatingState::Run(i) = self.operating_state {
                if i != 0 {
                    let interval = time::Duration::from_micros(i as u64);
                    thread::sleep(interval);
                }
            }
        }
    }
}
