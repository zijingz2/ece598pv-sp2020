use super::hash::{Hashable, H256};

/// A Merkle tree.
#[derive(Debug, Default)]
struct MerkleTreeNode {
    idx : usize,            // Node has unique ID
    hash : H256,
    parent : Option<usize>, // Parent ID
    left : Option<usize>,      // left child ID
    right : Option<usize>,     // right child ID
}

impl MerkleTreeNode {
    pub fn new(idx: usize, hash : H256) -> Self {
        Self {
            idx, 
            hash,
            parent : None,  
            left : None,  
            right : None, 
        }
    }
}

pub struct MerkleTree {
    node_list : Vec<MerkleTreeNode>, //Save all ndoes in Vec. 
    base_len  : usize,
    net_len   : usize,
}

impl MerkleTree {
    pub fn new<T>(data: &[T]) -> Self where T: Hashable, {
        // Starting from the bottom level of MerkleTree, fill in the Vec : node_list
        let mut dtlst = Vec::new();
        let i = 0;
        let lens = data.len();
        // First level
        if lens == 0 {
            let new_mt = MerkleTree{node_list : dtlst, base_len : 0, net_len : 0};
            return new_mt;
        }
        for t in data {
            dtlst.push(MerkleTreeNode::new(i,t.hash()));
        }
        // Second and higher level
        let mut idx_sdt = 0;        // starting position. (marker, iter)
        let mut idx_end;        // ending position.   (marker)
        let mut lvl_end = lens - 1; // ending position.   (iter)
        let mut ctx      : ring::digest::Context;
        let mut ctx_hash : ring::digest::Digest;
        let mut ctx_h256 : H256;

        while lvl_end - idx_sdt > 0 {        // Until one element left, idx_sdt=lvl_end
            idx_end = lvl_end;
            while idx_sdt <= idx_end {
                ctx = ring::digest::Context::new(&ring::digest::SHA256);
                ctx.update(dtlst[idx_sdt].hash.as_ref());
                if idx_sdt + 1 <= idx_end {
                    ctx.update(dtlst[idx_sdt+1].hash.as_ref()); // calculate hash, create parent node
                    ctx_hash = ctx.finish();
                    ctx_h256 = H256::from(ctx_hash); 
                    dtlst.push(MerkleTreeNode::new(i,ctx_h256));
                    dtlst[lvl_end+1].parent=None;               // update links
                    dtlst[lvl_end+1].left=Some(idx_sdt);
                    dtlst[lvl_end+1].right=Some(idx_sdt+1);
                    dtlst[idx_sdt].parent=Some(lvl_end+1);
                    dtlst[idx_sdt+1].parent=Some(lvl_end+1);
                    idx_sdt = idx_sdt + 2;                      // update loop
                } else {
                    ctx.update(dtlst[idx_sdt].hash.as_ref());   // calculate hash by duplicating self, create parent node
                    ctx_hash = ctx.finish();
                    ctx_h256 = H256::from(ctx_hash); 
                    dtlst.push(MerkleTreeNode::new(i,ctx_h256));
                    dtlst[lvl_end+1].parent=None;               // update links
                    dtlst[lvl_end+1].left=Some(idx_sdt);
                    dtlst[lvl_end+1].right=Some(idx_sdt);
                    dtlst[idx_sdt].parent=Some(lvl_end+1);
                    idx_sdt = idx_sdt + 1;                      // update loop
                }
                lvl_end = lvl_end + 1;
            }
        }
        let new_mt = MerkleTree{node_list : dtlst, base_len : lens, net_len : lvl_end};
        new_mt
    }

    pub fn root(&self) -> H256 {
        if self.base_len == 0{
            let mh : [u8; 32] = [0; 32];
            return H256::from(mh);
        }
        self.node_list[self.net_len].hash
    }

    /// Returns the Merkle Proof of data at index i
    pub fn proof(&self, index: usize) -> Vec<H256> {
        let mut idx : usize = index;
        let mut res : Vec<H256> = vec![];
        while let Some(x) = self.node_list[idx].parent {
            if self.node_list[x].left.expect("left child not exist") == idx {
                /* Change to match for return type Option*/
                res.push(self.node_list[self.node_list[x].right.expect("right child not exist")].hash);
            } else {
                /* Change to match for return type Option*/
                res.push(self.node_list[self.node_list[x].left.expect("left child not exist")].hash);
            }
            idx = x;
        }
        res
    }
}

/// Verify that the datum hash with a vector of proofs will produce the Merkle root. Also need the
/// index of datum and `leaf_size`, the total number of leaves.
pub fn verify(root: &H256, datum: &H256, proof: &[H256], index: usize, leaf_size: usize) -> bool {
    let mut ctx : ring::digest::Context;
    let mut ctx_hash : ring::digest::Digest;
    let mut datum_m : H256 = *datum;
    let mut idx = index + 1;
    for x in 0..log2n(leaf_size) {
        ctx = ring::digest::Context::new(&ring::digest::SHA256);
        if idx % 2 !=0 {
            ctx.update(datum_m.as_ref());
            ctx.update(proof[x].as_ref());
            idx = idx / 2 + 1;
        } else {
            ctx.update(proof[x].as_ref());
            ctx.update(datum_m.as_ref());
            idx = idx / 2;
        }
        ctx_hash = ctx.finish();
        datum_m = H256::from(ctx_hash); 
    }
    root.as_ref() == datum_m.as_ref()
}

pub fn log2n(int_in : usize) -> usize {
    let mut n = int_in; // only works for positive integer larger than 0
    let mut i = 0;
    while n > 1 {
        if n % 2 !=0 {
            n = n / 2 + 1;
        } else {
            n = n / 2;
        }
        i = i + 1;
    }
    i
}

#[cfg(test)]
mod tests {
    use crate::crypto::hash::H256;
    use super::*;

    macro_rules! gen_merkle_tree_data {
        () => {{
            vec![
                (hex!("0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d")).into(),
                (hex!("0101010101010101010101010101010101010101010101010101010101010202")).into(),
            ]
        }};
    }
    macro_rules! gen_merkle_tree_data_six_nodes {
        () => {{
            vec![
                (hex!("0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d")).into(),
                (hex!("0101010101010101010101010101010101010101010101010101010101010202")).into(),
                (hex!("0202020202020202020202020202020202020202020202020202020202020303")).into(),
                (hex!("0303030303030303030303030303030303030303030303030303030303030404")).into(),
                (hex!("0404040404040404040404040404040404040404040404040404040404040505")).into(),
                (hex!("0101010101010101010101010101010101010101010101010101010101010202")).into(),
            ]
            /*
            b69566be6e1720872f73651d1851a0eae0060a132cf0f64a0ffaea248de6cba0 Level 0
            965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f
            888b74a5c3cbf32fe9193e6528fb5f3411b0a1b30d6e41f999864cb46e8c7ecf
            50750028eed7a9e507a77313744de29be7b2c2e5cb45f454f4c7fada68b8b3bc
            dc92a85157bdf6c3627880861ad721b8f4eb7a574388967e782abde288bc0f30
            965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f

            6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920 Level 1
            44822f95124773b6c6a3dcf018c428648b0291d2a02f97e67460cf028a39a765
            6698744a28f49e74f24d64d662f4964ef44c005081fd49693044209d5109a09f

            7a59f737f7e9d2151c04346df096e9c582581ea7dcd8b5f8f6b458a7d01b9531 Level 2
            f61bcbac865d96120e13559b53514912b4a092d798354d15e05394ff240dc788

            c37e8236cc37439c891e3b91973d0a8d71dffb466238fd2e6067f7e2d64033f4 Root
            */
        }};
    }

    #[test]
    fn root() {
        let input_data: Vec<H256> = gen_merkle_tree_data_six_nodes!();
        let merkle_tree = MerkleTree::new(&input_data);
        let root = merkle_tree.root();
        assert_eq!(
            root,
            (hex!("c37e8236cc37439c891e3b91973d0a8d71dffb466238fd2e6067f7e2d64033f4")).into()
        );
        // "b69566be6e1720872f73651d1851a0eae0060a132cf0f64a0ffaea248de6cba0" is the hash of
        // "0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d"
        // "965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f" is the hash of
        // "0101010101010101010101010101010101010101010101010101010101010202"
        // "6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920" is the hash of
        // the concatenation of these two hashes "b69..." and "965..."
        // notice that the order of these two matters
    }

    #[test]
    fn proof() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(0);
        assert_eq!(proof,
                   vec![hex!("965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f").into()]
        );
        // "965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f" is the hash of
        // "0101010101010101010101010101010101010101010101010101010101010202"
    }

    #[test]
    fn verifying() {
        let input_data: Vec<H256> = gen_merkle_tree_data_six_nodes!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(0);
        assert!(verify(&merkle_tree.root(), &input_data[0].hash(), &proof, 0, input_data.len()));
    }
}
