use std::collections::HashMap;
use crate::transaction::{SignedTransaction,Transaction,sign};
use crate::crypto::hash::{H256, H160, Hashable};
use std::time;
use log::{info};
use crate::network::message::Message;
use crate::network::server::Handle as ServerHandle;
use crate::blockchain::Blockchain;
use ring::signature::{Ed25519KeyPair, KeyPair};

use std::thread;
use std::sync::{Arc,Mutex};
use rand::Rng;


pub struct Context {
    server: ServerHandle,
    blkchain : Arc<Mutex<Blockchain>>,
    mempool : Arc<Mutex<HashMap<H256,SignedTransaction>>>,
    statemap : Arc<Mutex<HashMap<H256, HashMap<(H256, u32), (H160, u32)>>>>,
    veckey : Arc<Mutex<Vec<[u8;100]>>>,
    key_len : usize,
}

pub fn new(
    server: &ServerHandle,
    blockchain: &Arc<Mutex<Blockchain>>,
    mempool : &Arc<Mutex<HashMap<H256,SignedTransaction>>>,
    statemap : &Arc<Mutex<HashMap<H256, HashMap<(H256, u32), (H160, u32)>>>>,
    veckey : &Arc<Mutex<Vec<[u8;100]>>>,
    key_len : usize,
) -> Context {
    Context {
        server: server.clone(),
        blkchain : Arc::clone(blockchain),
        mempool : Arc::clone(mempool),
        statemap : Arc::clone(statemap),
        veckey : Arc::clone(veckey),
        key_len : key_len,
    }
}

impl Context {
    pub fn start(self) {
        thread::Builder::new()
        .name("gen".to_string())
        .spawn(move || {
            self.gen_loop();
        })
        .unwrap();
    info!("generator ready.");
        
    }

    fn gen_loop(& self) {
        loop {
            let trans_num = self.mempool.lock().unwrap().len();
            if trans_num < 4  {
                let veckey = self.veckey.lock().unwrap();
                let account_number = veckey.len();
                if account_number > 1 {
                    let mut rng = rand::thread_rng();
                    let blkchn = self.blkchain.lock().unwrap();
                    let statemap = self.statemap.lock().unwrap();
                    let state = &statemap[&blkchn.tip()];
                    let state_len = state.len();
                    if state_len > 2 {
                        let inid : u32 = rng.gen_range(0, state_len as u32);
                        let ouid : u32 = rng.gen_range(0, account_number as u32);
                        let mut index : u32 = 0;
                        let mut found : u32 = 0;
                        let mut rand_input : (H256, u32) = (H256::from(&[0u8;32]),0);
                        let mut rand_output : (H160, u32) = (H160::from(&H256::from(&[0u8;32])),0);
                        for (key,val) in state.iter() {
                            index = index + 1;
                            if index == inid {
                                found = 1;
                                rand_input = Clone::clone(&key);
                                rand_output = Clone::clone(&val);
                                let mmp = self.mempool.lock().unwrap();
                                for (_keym, valm) in mmp.iter() {
                                    if rand_input.0 == valm.transaction.input_hash[0] && rand_input.1 == valm.transaction.input_index[0] {
                                        found = 0;
                                    }
                                }
                            }
                        }
                        if found == 1 {
                            // generate output address from self.veckey[ouid]
                            let mut out_addr : H160 = H160::from(&H256::from(&[0u8;32]));
                            let mut in_key : Ed25519KeyPair = Ed25519KeyPair::from_pkcs8(&veckey[0][0..self.key_len]).unwrap(); // possible uninitialized??
                            for i in 0..account_number {
                                let key_byte = veckey[i];
                                let key = Ed25519KeyPair::from_pkcs8(&key_byte[0..self.key_len]).unwrap();
                                let mut addr_byte : [u8 ; 32] = [0 ; 32];
                                addr_byte.copy_from_slice(key.public_key().as_ref());
                                let addr = H160::from(&H256::from(&addr_byte));
                                if addr == rand_output.0 { // find sender
                                    in_key = key;
                                }
                                if i == ouid as usize { // find receiver
                                    out_addr = Clone::clone(&addr);
                                }
                            }
                            let in_pubkey = in_key.public_key().as_ref().to_vec();
                            let money = rng.gen_range(0,rand_output.1+1 as u32);
                            let trans : Transaction;
                            if money == rand_output.1 {
                                trans = Transaction::new(rand_input.0, rand_input.1, vec![out_addr], vec![money]);
                            } else {
                                trans = Transaction::new(rand_input.0, rand_input.1, vec![rand_output.0, out_addr], vec![rand_output.1-money, money]);
                            }
                            
                            info!("generating transaction: addr {:?} send {} coins to addr {:?}.", rand_output.0, money, out_addr);
                            let signa = sign(&trans, &in_key).as_ref().to_vec();
                            let transhash = Hashable::hash(&trans);
                            // sign this with the key of sender                        
                            let signed_trans = SignedTransaction{transaction : trans, pub_key : in_pubkey, signature: signa, trans_hash : transhash};
                            // add to mempool, and broadcast
                            self.server.broadcast(Message::NewTransactionHashes(vec![transhash]));
                            self.mempool.lock().unwrap().insert(transhash, Clone::clone(&signed_trans));
                        }
                    }
                } else {
                    for i in 0..account_number {
                        let mut msgs : [u8; 32] = [0; 32];
                        msgs.copy_from_slice(&veckey[i][0..32]);
                        self.server.broadcast(Message::SharePKCS8Keys(msgs));
                    }
                }
            }
            thread::sleep(time::Duration::from_millis(2000));
        }
    }
}
