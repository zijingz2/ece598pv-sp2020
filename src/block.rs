use serde::{Serialize, Deserialize};
use crate::crypto::hash::{H256, Hashable};
use crate::transaction::{SignedTransaction};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Block {
    pub head : Header,
    pub content : Vec<SignedTransaction>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Header {
    pub parent : H256,
    pub nonce  : u32,
    pub difficulty : H256,
    pub timestamp : i64,
    pub merkle_root : H256,

}

impl Hashable for Block {
    fn hash(&self) -> H256 {
        Hashable::hash(&self.head)
    }
}

impl PartialEq for Block {
    fn eq(&self, other: &Self) -> bool {
        Hashable::hash(self) == Hashable::hash(other)
    }
}
impl Eq for Block {}

impl Hashable for Header {
    fn hash(&self) -> H256 {
        let mut ctx = ring::digest::Context::new(&ring::digest::SHA256);
        let self_bytes = bincode::serialize(&self).unwrap();
        ctx.update(&self_bytes);
        H256::from(ctx.finish())
    }
}

#[cfg(any(test, test_utilities))]
pub mod test {
    use super::*;
    use crate::crypto::hash::H256;
    use crate::crypto::merkle::{MerkleTree};
    use rand::Rng;

    pub fn generate_random_block(parent: &H256) -> Block {
        let mut rng = rand::thread_rng();
        let nonce : u32 = rng.gen();
        let dif : [u8; 32] = [8 ; 32];
        let tmsdp = chrono::Utc::now().timestamp_nanos(); // change to milisecond
        let tt : Vec<SignedTransaction> = vec![];
        let mk = MerkleTree::new(&tt);
        let mkrt : H256 = mk.root();
        let hdif = H256::from(dif);
        let hd = Header{parent : *parent, nonce : nonce, difficulty : hdif , timestamp : tmsdp, merkle_root : mkrt};
        let bk = Block{head : hd, content : tt};
        // println!("{:?}", bk);
        bk
    }
}
